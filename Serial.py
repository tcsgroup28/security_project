#!/usr/bin/env python3
import serial
import sqlite3
import hashlib
import datetime
import time
conn=sqlite3.connect('project.db')
#conn=sqlite3.connect('/Users/Zombarian/Documents/security/project.db',check_same_thread=False)
c=conn.cursor()
days= {0:'Monday',1:'Tuesday',2:'Wednsday',3:'Thursday',4:'Friday',5:'Saturday',6:'Sunday'}
def data_entry(value, stID, name):
    c.execute("INSERT INTO Students(studentID, fullName,cardID) VALUES(?,?,?)",(stID,name,value,))
    conn.commit()
def attendance_entry(inpt,ser):
    unix= time.time()
    dateNtime = str(datetime.datetime.fromtimestamp(unix).strftime('%Y-%m-%d %H:%M:%S'))
    c.execute('SELECT studentID FROM Students WHERE cardID = ?',(inpt,))
    stID= data=c.fetchone()[0]
    date = dateNtime[:10]
    Time = dateNtime[11:]
    Time='10:52:29'
    date='2017-10-25'
    c.execute("SELECT ID FROM Schedule as s WHERE s.date==? AND ? >= s.startTime AND ? <=s.endTime",(date,Time,Time,))
    temp=c.fetchall()
    if temp:
        classID=temp[0][0]
    else: classID=0 # If student registered outside a lesson, make classID=0
    if check_Attendance(stID,classID):
        c.execute("INSERT INTO Attendance(studentID, SignInTime, classID) VALUES(?,?,?)",(stID,Time,classID,))
        conn.commit()
        print('Signed in!')
        ser.write('I'.encode('utf-8'))
    elif check_signOff(stID,classID):
        c.execute("UPDATE Attendance SET SignOffTime= ? WHERE studentID==? AND classID== ?",(Time,stID,classID,))
        conn.commit()
        print('Signed off!')
        ser.write('O'.encode('utf-8'))
    else:
        ser.write('N'.encode('utf-8'))
        print('Already registered!')
def check_Attendance(stID,classID):
    c.execute("SELECT count(*) FROM Attendance WHERE studentID == ? AND classID== ?", (stID,classID,))
    data=c.fetchone()[0]
    if data==0:
        return True
    else:
        return False
def check_signOff(stID,classID):
    c.execute("SELECT count(*) FROM Attendance WHERE studentID == ? AND classID== ? AND SignOffTime IS NOT NULL", (stID,classID,))
    data=c.fetchone()[0]
    if data==0:
        return True
    else:
        return False
def create_tables():
    #c.execute('DROP TABLE IF EXISTS Cards')
    #c.execute('CREATE TABLE IF NOT EXISTS Cards(UFID text, studentID text)')
    #c.execute('DROP TABLE IF EXISTS Students')
    c.execute('CREATE TABLE IF NOT EXISTS Students(studentID text,fullName text, cardID text,password text, PRIMARY KEY(studentID))')
    #c.execute('DROP TABLE IF EXISTS Attendance')
    c.execute('CREATE TABLE IF NOT EXISTS Attendance(studentID text,SignInTime text,SignOffTime, classID integer, PRIMARY KEY(studentID,classID))')
    c.execute('DROP TABLE IF EXISTS Classes')
#    c.execute('CREATE TABLE IF NOT EXISTS Classes(Name text, teacherName text, room text)')
   # c.execute('DROP TABLE IF EXISTS Schedule')
    c.execute('CREATE TABLE IF NOT EXISTS Schedule(ID integer, className text, startTime text, endTime text, date text,week integer, teacherName text, room text, activityType text,courseID INTEGER, PRIMARY KEY(ID))')
 #   c.execute('DROP TABLE IF EXISTS Courses')
    c.execute('CREATE TABLE IF NOT EXISTS Courses(courseID INTEGER, courseName text, PRIMARY KEY(courseID))')
    c.execute('CREATE TABLE IF NOT EXISTS Teachers(teacherID INTEGER, password text, fullName text, PRIMARY KEY(teacherID))')
    c.execute('CREATE TABLE IF NOT EXISTS Privileges(teacherID integer, courseID integer, PRIMARY KEY(teacherID,courseID))')
def check_entry(name):
    c.execute("SELECT count(*) FROM Students WHERE cardID = ?", (name,))
    data=c.fetchone()[0]
    if data==0:
        return True
    else:
        return False
def scan_loop():
    try:
        #ser = serial.Serial('/dev/cu.usbmodem1411', 9600)
        ser = serial.Serial('COM7', 9600)
        while 1:
            inpt=hashlib.sha224(ser.readline()).hexdigest()
            if check_entry(inpt):           # If the card is not already in the system
                print('New card detected!')
                stID=input('Card not bound to student! Please enter student id:')
                name=input('Also, student name:')
                data_entry(inpt,stID,name)       # Adds the card together with the studentID
                print('Card registered!')
                attendance_entry(inpt,ser)
            else:
                attendance_entry(inpt,ser)
    finally:
        print("Closing ports!")
        #c.close()
        #conn.close()


create_tables()
scan_loop()
