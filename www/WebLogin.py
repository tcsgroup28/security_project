
class Login:
    def GET(self):
        params = web.input()
        return render.login(params.get('returnURL'))

    def POST(self):
        params = web.input()
        conn = sqlite3.connect(dbPath)
        c = conn.cursor()
        data = web.input()
        u = data.username
        p = hashlib.sha224((data.password).encode('utf-8')).hexdigest()

        if u[0] == "s": #Check if it is a student
            c.execute('SELECT * FROM Students as s WHERE s.studentID=? AND s.password=?', (u, p,))
        else:
            c.execute('SELECT * FROM Teachers as t WHERE t.teacherID=? AND t.password=?', (u, p,))

        user = c.fetchall()
        if user != []:
            print("Logged in!")
            if user[0][0][0] == 's': #first char of either student or teacher id
                session.user_id = u
                session.user_type = 'student'
            elif user[0][0][0] == 'm':
                session.user_id = u
                session.user_type = 'teacher'
            if params.get('returnURL'):
                return web.seeother(params.get('returnURL'))
            else: return "Logged in!"
        else:
            return render.login(params.get('returnURL'))

