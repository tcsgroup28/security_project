import hashlib
import sqlite3

import DatabaseHelper
import DateTimeUtils
import web
from web import ctx
#import Serial

dbPath='../project.db'

#Map urls with class names.
#Left column: URL; Right column: class name.
# So for example for a URL localhost:8080/viewAttendance, a class ViewAttendance will be used (all these classes are defined in this file).
urls = (
    '/viewAttendance', 'ViewAttendance',
    '/viewAttendance/', 'ViewAttendance',
    '/pickSession', 'SessionPicker',
    '/pickSession/', 'SessionPicker',
    '/login', 'Login',
    '/login/', 'Login',
    '/logout/', 'Logout',
    '/logout', 'Logout',
    '/', 'index',
    '/attendanceButtons','AttendanceButtons',
    '/attendanceButtons//','AttendanceButtons',
    '/AttendanceofStudents', 'AttendanceofStudents',
    '/AttendanceofStudents/', 'AttendanceofStudents',
)

#Actually starts a web server
app = web.application(urls, globals())

if web.config.get('_session') is None:
    session = web.session.Session(app, web.session.DiskStore('sessions'), {'user_id': '', 'user_type': ''})
    web.config._session = session
else:
    session = web.config._session


if __name__ == "__main__":
    app.run()


class index:
    def GET(self):
        return render.index()

class AttendanceofStudents:
    def GET(self):
        if logged_in():
            if get_user_type() == "teacher":
                conn = sqlite3.connect(dbPath)
                c = conn.cursor()
                c.execute('SELECT studentID,fullName from Students')
                userID = []
                name = []
                userData = c.fetchall()
                for user in userData:
                    userID.append(user[0])
                    name.append(user[1])
                attendance1 = []
                attendance2 = []
                attendance3 = []
                attendance4 = []
                attendance5 = []
                attendance6 = []
                act1 = 'Tutorial'
                act2 = 'Practical'
                act3 = 'Self study supervised'
                act4 = 'Other'
                act5= 'Project supervised'
                class1 = 'Math A & Math B1 (CS)'
                class2 = 'Pearl assignment'
                class3 = 'Pearl Tutorial'
                class4 = 'Project'
                i = 0
                while i < len(userID):
                    c.execute(
                        'SELECT count(*) from Attendance as a, Schedule as s WHERE a.studentID=? AND a.classID=s.ID AND s.activityType=? AND s.className=? GROUP BY studentID',
                        (userID[i], act1, class1,))
                    att = c.fetchall()
                    if att != []:
                        attendance1.append(att[0])
                    else:
                        att = [(0,)]
                        attendance1.append(att[0])
                    c.execute(
                        'SELECT count(*) from Attendance as a, Schedule as s WHERE a.studentID=? AND a.classID=s.ID AND s.activityType=? AND s.className=? GROUP BY studentID',
                        (userID[i], act3, class1,))
                    att = c.fetchall()
                    if att != []:
                        attendance2.append(att[0])
                    else:
                        att = [(0,)]
                        attendance2.append(att[0])
                    c.execute(
                        'SELECT count(*) from Attendance as a, Schedule as s WHERE a.studentID=? AND a.classID=s.ID AND s.activityType=? AND s.className=? GROUP BY studentID',
                        (userID[i], act1, class3,))
                    att = c.fetchall()
                    if att != []:
                        attendance5.append(att[0])
                    else:
                        att = [(0,)]
                        attendance5.append(att[0])
                    c.execute(
                        'SELECT count(*) from Attendance as a, Schedule as s WHERE a.studentID=? AND a.classID=s.ID AND s.activityType=? AND s.className=? GROUP BY studentID',
                        (userID[i], act4, class1,))
                    att = c.fetchall()
                    if att != []:
                        attendance3.append(att[0])
                    else:
                        att = [(0,)]
                        attendance3.append(att[0])
                    c.execute(
                        'SELECT count(*) from Attendance as a, Schedule as s WHERE a.studentID=? AND a.classID=s.ID AND s.activityType=? AND s.className=? GROUP BY studentID',
                        (userID[i], act2, class2,))
                    att = c.fetchall()
                    if att != []:
                        attendance4.append(att[0])
                    else:
                        att = [(0,)]
                        attendance4.append(att[0])
                    c.execute(
                        'SELECT count(*) from Attendance as a, Schedule as s WHERE a.studentID=? AND a.classID=s.ID AND s.activityType=? AND s.className=? GROUP BY studentID',
                        (userID[i], act5, class4,))
                    att = c.fetchall()
                    if att != []:
                        attendance6.append(att[0])
                    else:
                        att = [(0,)]
                        attendance6.append(att[0])
                    i = i + 1
                c.close()
                conn.close()
                return render.teacher(userID, name, attendance1, attendance2, attendance5, attendance3, attendance4,attendance6)
            else:
                return render.access_denied()
        else:
            return web.seeother(getHomePath() + "/login?returnURL=AttendanceofStudents")
class AttendanceButtons:
    def GET(self):
        params = web.input()
        if params.get('button')=='delete':
            conn = sqlite3.connect(dbPath)
            c = conn.cursor()
            c.execute("DELETE FROM Attendance WHERE studentID=? AND classID=?",(params.get('key'),params.get('sessionId'),) )
            conn.commit()
            c.close()
            conn.close()
            return web.seeother(getHomePath()+'/viewAttendance?sessionId='+params.get('sessionId'))
        elif params.get('button')=='signoff':
            conn = sqlite3.connect(dbPath)
            c = conn.cursor()
            c.execute("UPDATE Attendance SET SignOffTime=? WHERE studentID=? AND classID=?",
                      (DateTimeUtils.returnCurrentTime(),params.get('key'), params.get('sessionId'),))
            conn.commit()
            c.close()
            conn.close()
            return web.seeother(getHomePath() + '/viewAttendance?sessionId=' + params.get('sessionId'))
        else:
            return web.seeother(getHomePath())


class ViewAttendance:
    def GET(self):
        params = web.input() #incoming GET parameters (dictionary)
        if logged_in():
            if session.user_type == 'teacher':
                if params.get('sessionId'): #if sessionId GET parameter is passed
                    sessionId = params.get('sessionId') #id of a session (i.e. a lesson)
                    # get session info from database (dictionary)
                    sessionInfo = DatabaseHelper.getSessionDetails(dbPath, sessionId)

                    # format date
                    sessionInfo['date'] = DateTimeUtils.readableDate(sessionInfo['date'])

                    if not sessionInfo.get('teacherName'): sessionInfo['teacherName'] = 'No teacher' #If there is no teacher, replace empty value with 'No teacher'
                    if not sessionInfo.get('room'): sessionInfo['room'] = 'No room'
                    sessionInfo['userType']=get_user_type()
                    # a list of all student names, numbers, sign in and sign of time.
                    # queried from the database.
                    attendanceDict = DatabaseHelper.getAttendance(dbPath, sessionId)

                    # Outputs a template 'view_attendance' with the parameters given in brackets.
                    # Parameters are used in the template, see templetes/view_attendance.html
                    privilege=False
                    conn = sqlite3.connect(dbPath)
                    c = conn.cursor()
                    c.execute("SELECT * FROM Schedule as s, Privileges as p WHERE s.courseID=p.courseID AND s.ID=? AND p.teacherID=?",(sessionId,web.config._session.get('user_id'),))
                    if len(c.fetchall())>0:
                        privilege=True
                    return render.view_attendance(sessionId, attendanceDict, sessionInfo,[],privilege)
                else: #if sessionId GET param is NOT passed, then output the SessionPicker.
                    #we imitate a GET request to SessionPicker, hence `GET`. We also pass a 'viewAttendance' as a returnURL for SessionPicker to send the selected session ID to.
                    return SessionPicker.GET(self, 'viewAttendance')
            elif session.user_type=='student':
                try:

                    conn = sqlite3.connect(dbPath)
                    c = conn.cursor()
                    studentID=web.config._session.get('user_id')
                    studentInfo={}
                    attendances=[]
                    for lessons in [['Math A & Math B1 (CS)','Tutorial'],['Math A & Math B1 (CS)','Self study supervised'],['Diagnostic test','Colstruction'],['Pearl assignment','Practical'],['Pearl tutorial','Tutorial'],['Project','Project supervised']]:
                        c.execute('SELECT count(*) from Attendance as a, Schedule as s WHERE a.studentID=? AND a.classID=s.ID AND s.activityType=? AND s.className=?',(studentID,lessons[1],lessons[0],))
                        attendances.append(c.fetchall()[0][0])
                    studentInfo['stats']=attendances
                    c.execute('SELECT SignInTime,SignOffTime,date,room, activityType,className FROM Attendance as a, Schedule as s WHERE a.studentID=? AND a.classID=s.ID',(studentID,))
                    info=c.fetchall()
                    studentInfo['instances']=info
                    c.execute('SELECT fullName FROM Students WHERE studentID=?',(studentID,))
                    name=c.fetchall()
                    studentInfo['name']=name[0][0]
                    return render.student_attendance(studentID, studentInfo)
                finally:
                    c.close()
                    conn.close()
        else:
            return web.seeother(getHomePath() + "/login?returnURL=viewAttendance")

    def POST(self):
        params = web.input()
        conn = sqlite3.connect(dbPath)
        c = conn.cursor()
        data = web.input()
        stID = data.studentID
        c.execute("SELECT * FROM Students where studentID==?",(stID,))
        student=c.fetchall()
        if len(student)==0:
            print('Student not found!')
        else:
            c.execute("SELECT * FROM Attendance WHERE studentID==? AND  classID==?",(stID,params.get('sessionId'),))
            if len(c.fetchall())==0:
                c.execute("INSERT INTO Attendance(studentID,SignInTime,classID) VALUES(?,?,?)",(stID,DateTimeUtils.returnCurrentTime(),params.get('sessionId'),))
                conn.commit()
            else:
                print('Student already registered!')
        #else:
            #c.execute("INSERT INTO")
        c.close()
        conn.close()
        return ViewAttendance.GET(self)


#From a list of sessions, find ones which are currently running (according to the time)
def getActiveSessionIDs(date, timetable):
    array=[]
    for session in timetable:
        if DateTimeUtils.isSessionOngoing(DateTimeUtils.strToDate(date), session.get('startTime'), session.get('endTime')):
            array.append(session.get('ID'))
    return array

class SessionPicker:
    def GET(self, returnURL=None):
        params = web.input()  # incoming GET parameters (dictionary)
        timetable = []
        errors = []
        date = None
        activeSessionIDs = ""
        if attrProvided('returnURL', params) and returnURL is None:
            returnURL = params.get('returnURL')
        if attrProvided('date', params):
            date = params.get('date')
            timetable = DatabaseHelper.getTimetable(dbPath, date)
            activeSessionIDs = getActiveSessionIDs(date, timetable)
            if len(timetable) == 0:
                errors.append("There are no sessions for this date")
                date = None
        return render.session_picker(errors, date, timetable, DateTimeUtils.todayDate(),
                                              activeSessionIDs, returnURL)

def logged_in():
    return web.config._session.get('user_id', None) and web.config._session.get('user_type', None)

def get_user_type():
    if logged_in(): return web.config._session.get('user_type')

class Logout:
    def GET(self):
        params = web.input()
        session.kill()
        web.config._session.user_id = ""
        web.config._session.user_type = ""
        if params.get('returnURL'):
            return web.seeother(params.get('returnURL'))
        else: return web.seeother(getHomePath())

class Login:
    def GET(self):
        params = web.input()
        return render.login(params.get('returnURL'))

    def POST(self):
        params = web.input()
        conn = sqlite3.connect(dbPath)
        c = conn.cursor()
        data = web.input()
        u = data.username
        p = hashlib.sha256(data.hashedpass.encode('utf-8')).hexdigest()

        if u[0] == "s": #Check if it is a student
            c.execute('SELECT * FROM Students as s WHERE s.studentID=? AND s.password=?', (u, p,))
        else:
            c.execute('SELECT * FROM Teachers as t WHERE t.teacherID=? AND t.password=?', (u, p,))

        user = c.fetchall()
        if user != []:
            print("Logged in!")
            if user[0][0][0] == 's': #first char of either student or teacher id
                session.user_id = u
                session.user_type = 'student'
            elif user[0][0][0] == 'm':
                session.user_id = u
                session.user_type = 'teacher'
            if params.get('returnURL'):
                return web.seeother(params.get('returnURL'))
            else: return "Logged in!"
        else:
            return render.login(params.get('returnURL'))


#A helper function to determine whether an attribute is inside a params dictionary.
def attrProvided(attribute, params):
    return attribute in params and len(params[attribute]) > 0

def getHomePath():
    if 'home' in ctx:
        return ctx.home
    else:
        return ""

#Additional variables which will be accessible from templates #
template_globals = {
    'root': getHomePath(), #full path of the index. Like 'localhost:8080'
    'user_id': lambda: web.config._session.get('user_id'), #id of logged in user. lambda is a function
    'user_type': lambda: web.config._session.get('user_type'),
    'used_session_id': lambda: None

}

# render is just a variable which lets you fill in the templtes.
# Arguments are: 1: The path of templates directory; 2: 'functions' which will be available in the template.
# We need 1 function app_path to be able to refer to the root url in the templates.
# (Something complicated is happening under the hood).
# Refer to web.py documentation if you want to know how it works.
render = web.template.render('templates/', globals=template_globals, base='base_layout')