import web
import sqlite3
import hashlib


class AttendanceofStudents:
    def GET(self):
        conn = sqlite3.connect('project.db')
        c = conn.cursor()
        c.execute('SELECT studentID from Students')
        userID = c.fetchall()
        c.execute('SELECT fullName from Students')
        name = c.fetchall()
        attendance1 = []
        attendance2 = []
        attendance3 = []
        attendance4 = []
        attendance5 = []
        act1 = 'Tutorial'
        act2 = 'Practical'
        act3 = 'Self study supervised'
        act4 = 'Other'
        class1 = 'Math A & Math B1 (CS)'
        class2 = 'Pearl assignment'
        class3 = 'Pearl Tutorial'
        i = 0
        while i < len(userID):
            c.execute(
                'SELECT count(*) from Attendance as a, Schedule as s WHERE a.studentID=? AND a.classID=s.ID AND s.activityType=? AND s.className=? GROUP BY studentID',
                (userID[i][0], act1, class1,))
            att = c.fetchall()
            if att != []:
                attendance1.append(att[0])
            else:
                att = [(0,)]
                attendance1.append(att[0])
            c.execute(
                'SELECT count(*) from Attendance as a, Schedule as s WHERE a.studentID=? AND a.classID=s.ID AND s.activityType=? AND s.className=? GROUP BY studentID',
                (userID[i][0], act3, class1,))
            att = c.fetchall()
            if att != []:
                attendance2.append(att[0])
            else:
                att = [(0,)]
                attendance2.append(att[0])
            c.execute(
                'SELECT count(*) from Attendance as a, Schedule as s WHERE a.studentID=? AND a.classID=s.ID AND s.activityType=? AND s.className=? GROUP BY studentID',
                (userID[i][0], act1, class3,))
            att = c.fetchall()
            if att != []:
                attendance5.append(att[0])
            else:
                att = [(0,)]
                attendance5.append(att[0])
            c.execute(
                'SELECT count(*) from Attendance as a, Schedule as s WHERE a.studentID=? AND a.classID=s.ID AND s.activityType=? AND s.className=? GROUP BY studentID',
                (userID[i][0], act4, class1,))
            att = c.fetchall()
            if att != []:
                attendance3.append(att[0])
            else:
                att = [(0,)]
                attendance3.append(att[0])
            c.execute(
                'SELECT count(*) from Attendance as a, Schedule as s WHERE a.studentID=? AND a.classID=s.ID AND s.activityType=? AND s.className=? GROUP BY studentID',
                (userID[i][0], act1, class3,))
            att = c.fetchall()
            if att != []:
                attendance4.append(att[0])
            else:
                att = [(0,)]
                attendance4.append(att[0])
            i = i + 1
        return render.teacher(userID, name, attendance1, attendance2, attendance5, attendance3, attendance4,attendance5)


render = web.template.render('templates/')

urls = (
    '/AttendanceofStudents', 'AttendanceofStudents'
)

if __name__ == "__main__":
    app = web.application(urls, globals())
    session = web.session.Session(app, web.session.DiskStore('sessions'), initializer={'user': ''})
    app.run()
