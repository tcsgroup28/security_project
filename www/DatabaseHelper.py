import sqlite3

'''
    This is a utility class which helps to perform common interaction with the database.
'''

#Gets the schedule for the given date.
# Returns a list of dictionaries, where each dictionary describes a session (a lesson).
def getTimetable(dbPath, date):
    timetable = []
    try:
        conn = sqlite3.connect(dbPath)
        c = conn.cursor()
        c.execute("SELECT DISTINCT ID, className, startTime,endTime,teacherName, room, activityType FROM Schedule WHERE Schedule.date=? ORDER BY startTime", (date,))
        result = c.fetchall()
        if result:
            for session in result:
                c.execute("SELECT count(*) FROM Attendance WHERE classId=? AND SignOffTime IS NULL", (session[0],))
                number=c.fetchall()
                timetable.append({'ID' : session[0], 'className': session[1], 'startTime': session[2], 'endTime': session[3], 'teacherName': session[4], 'room': session[5], 'activityType': session[6],'signedIn':number[0][0]})
        return timetable
    finally:
        conn.close()

#Gets a list of signins and singoffs for a given sessionId.
#Returns a list of dictionaries
def getAttendance(dbPath, sessionId):
    attendance = {}
    try:
        conn = sqlite3.connect(dbPath)
        c = conn.cursor()
        c.execute("SELECT a.SignInTime,a.SignOffTime, s.fullName, a.studentID FROM Attendance as a, Students as s WHERE classId=? AND s.studentID==a.studentID", (sessionId,))
        result = c.fetchall()
        attendance['signedInStudents'] = []
        if result:
            attendance['signedInCount'] = count_students(result)
            for studentEntry in result:
                attendance['signedInStudents'].append({'studentID': studentEntry[3], 'studentName': studentEntry[2], 'SignInTime': studentEntry[0], 'SignOffTime': studentEntry[1]})
        else:
            attendance['signedInCount'] = 0
        return attendance


    finally:
        conn.close()

def count_students(result):
    i=0
    for studentEntry in result:
        if not studentEntry[1]:
            i+=1
    return i

#Gets all information about a session with given ID. Returns a dictionary.
def getSessionDetails(dbPath, sessionId):
    details = {}
    try:
        conn = sqlite3.connect(dbPath)
        c = conn.cursor()
        c.execute("SELECT className, startTime, endTime, teacherName, room, activityType, date from Schedule where ID=?", (sessionId,))
        result = c.fetchall()
        if result:
            details['sessionId'] = sessionId
            details['className'] = result[0][0]
            details['startTime'] = result[0][1]
            details['endTime'] = result[0][2]
            details['teacherName'] = result[0][3]
            details['room'] = result[0][4]
            details['activityType'] = result[0][5]
            details['date'] = result[0][6]
            return details
    finally:
        conn.close()
