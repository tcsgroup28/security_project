import hashlib
import json
import sqlite3
from threading import Thread, Lock

import web
import os

import time
from serial import SerialException
from web import ctx
import DatabaseHelper, DateTimeUtils, Serial2
from Web import getActiveSessionIDs
web.config.debug = False

dbPath = "../project.db"


def getHomePath():
    if 'home' in ctx:
        return ctx.home
    else:
        return ""

#Additional variables which will be accessible from templates #
template_globals = {
    'root': getHomePath(), #full path of the index. Like 'localhost:8080'
    'user_id': lambda: session.get('user_id'), #id of logged in user. lambda is a function
    'user_type': lambda: session.get('user_type'),
    'used_session_id': lambda: used_session_id(),
    'session_details': lambda id: DatabaseHelper.getSessionDetails(dbPath, id)

}

# render is just a variable which lets you fill in the templtes.
# Arguments are: 1: The path of templates directory; 2: 'functions' which will be available in the template.
# We need 1 function app_path to be able to refer to the root url in the templates.
# (Something complicated is happening under the hood).
# Refer to web.py documentation if you want to know how it works.
render = web.template.render('templates/', globals=template_globals, base='base_layout')

urls = (
    '/', 'Index',
    '/login', 'Login',
    '/logout', 'Logout',
    '/scanEvents/', 'ScanEventProvider',
    '/stopTracking/', 'StopTracking',
    '/stopTracking', 'StopTracking',
    '/registerCard', 'RegisterCard',
    '/proceedScan', 'ProceedScan',
    '/totalSignoff', 'TotalSignoff',
    '/notifyAlive', 'NotifyAlive'
)

app = web.application(urls, globals())

session = web.session.Session(app, web.session.DiskStore('sessions'), {'user_id': None, 'user_type': None})


def logged_in():
    return session.get('user_id', None) and session.get('user_type', None)


def get_user_type():
    if logged_in(): return session.get('user_type')


file_lock = Lock()
def get_scan_events():
    file = None
    global last_alive
    Serial2.on_keep_alive()

    with file_lock:
        try:
            file = open("scan_events", "r")
            return json.loads(file.read())
        except:
            return []
        finally:
            if file is not None:
                file.close()
                os.remove("scan_events")

def add_scan_event(is_positive, text):
    events = get_scan_events()
    events.append([is_positive, text])
    file = None
    with file_lock:
        try:
            file = open("scan_events", "w")
            file.write(json.dumps(events))
        finally:
            if file is not None: file.close()


class ProceedScan:
    def GET(self):
        Serial2.make_sound("S")

class RegisterCard:
    def GET(self):
        params = web.input()
        if params.get('username') and params.get('password') and params.get('session_id') and params.get('card_id'):
            conn = None
            c = None
            try:
                conn = sqlite3.connect(dbPath)
                c = conn.cursor()
                hashedpwd = hashlib.sha256(params.get('password').encode('utf-8')).hexdigest()
                c.execute("UPDATE Students SET cardID = ? WHERE studentID=? AND password=?", (params.get('card_id'), params.get('username'), hashedpwd,))
                conn.commit()
                if c.rowcount > 0:
                    Serial2.process_rfid(params.get('card_id'), params.get('session_id'), conn, c)
                    #Serial2.make_sound('I')
                    return 1
                else:
                    Serial2.make_sound('N')
                    return 0

            except Exception as e:
                print(e)
                Serial2.make_sound('N')
                return 0
            finally:
                if c: c.close()
                if conn: conn.close()


class SessionPicker:
    def GET(self, returnURL=None):
        params = web.input()  # incoming GET parameters (dictionary)
        timetable = []
        errors = []
        date = None
        activeSessionIDs = ""
        if params.get('returnURL') and returnURL is None:
            returnURL = params.get('returnURL')
        if params.get('date'):
            date = params.get('date')
            timetable = DatabaseHelper.getTimetable(dbPath, date)
            activeSessionIDs = getActiveSessionIDs(date, timetable)
            if len(timetable) == 0:
                errors.append("There are no sessions for this date")
                date = None
        return render.session_picker(errors, date, timetable, DateTimeUtils.todayDate(),
                                              activeSessionIDs, returnURL)


class TotalSignoff:
    def GET(self, returnURL=""):
        if logged_in():
            if used_session_id():
                sId = used_session_id()
                conn = sqlite3.connect(dbPath)
                c = conn.cursor()
                currTime = DateTimeUtils.returnCurrentTime()
                c.execute("UPDATE Attendance SET SignOffTime=? WHERE classID=? AND SignOffTime is NULL", (currTime, sId,))
                conn.commit()
                add_scan_event(True, "Signed everybody off!")
        return web.seeother(getHomePath() + "/" + returnURL)


class StopTracking:
    def GET(self):
        print("Stop Tracking page!")
        if used_session_id():
            stop_tracking()
        return web.seeother("/")


def stop_tracking():
    Serial2.must_stop = True
    global scan_thread, _used_session_id
    scan_thread = None
    _used_session_id = -1


class Logout:
    def GET(self):
        print("Logout page!")
        params = web.input()
        session.kill()
        web.config._session.user_id = ""
        web.config._session.user_type = ""

        stop_tracking()

        if params.get('returnURL'):
            return web.seeother(params.get('returnURL'))
        else: return web.seeother(getHomePath())


class Login:
    def GET(self):
        params = web.input()
        return render.login(params.get('returnURL'))

    def POST(self):
        params = web.input()
        conn = sqlite3.connect(dbPath)
        c = conn.cursor()
        data = web.input()
        u = data.username
        p = hashlib.sha256(data.hashedpass.encode('utf-8')).hexdigest()

        if u[0] == "s": #Check if it is a student
            c.execute('SELECT * FROM Students as s WHERE s.studentID=? AND s.password=?', (u, p,))
        else:
            c.execute('SELECT * FROM Teachers as t WHERE t.teacherID=? AND t.password=?', (u, p,))

        user = c.fetchall()
        if user != []:
            print("Logged in!")
            if user[0][0][0] == 's': #first char of either student or teacher id
                session.user_id = u
                session.user_type = 'student'
            elif user[0][0][0] == 'm':
                session.user_id = u
                session.user_type = 'teacher'
            if params.get('returnURL'):
                return web.seeother(params.get('returnURL'))
            else: return "Logged in!"
        else:
            return render.login(params.get('returnURL'))


class ScanEventProvider:
    def GET(self):
        return json.dumps(get_scan_events())


def used_session_id():
    if scan_thread and scan_thread.isAlive():
        return _used_session_id


scan_thread = None
_used_session_id = -1

class Index:

    def GET(self):
        params = web.input()  # incoming GET parameters (dictionary)
        if logged_in():
            if get_user_type() == "teacher":
                if params.get('sessionId'):
                    sessionId = params.get('sessionId')
                    sessionDetails = DatabaseHelper.getSessionDetails(dbPath, sessionId)
                    sessionDetails['date'] = DateTimeUtils.readableDate(sessionDetails['date'])

                    global scan_thread
                    if not scan_thread or not scan_thread.isAlive() or used_session_id() != sessionId:
                        global _used_session_id
                        _used_session_id = sessionId
                        scan_thread = Thread(target=scan_loop_threaded, args=(sessionId,))
                        scan_thread.start()

                    return render.track_attendance(sessionDetails, )
                else:
                    return SessionPicker.GET(self, "")
            else:
                return render.access_denied()
        else:
            return web.seeother(getHomePath() + "/login?returnURL=/")


def scan_loop_threaded(session_id):
    try:
        Serial2.scan_loop(dbPath, session_id)
    except Exception as e:
        print(e)
        add_scan_event(False, "__fatal:Could not find Arduino RFID Scanner")


print("Please open the following URL in the browser to configure the scanner!")

if __name__ == "__main__":
    app.run()
