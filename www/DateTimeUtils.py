import datetime


def isSessionOngoing(date, startTimeStr, endTimeStr):
    startTime = strToTime(date, startTimeStr)
    endTime = strToTime(date, endTimeStr)
    now = datetime.datetime.now()
    #now = strToTime(datetime.datetime.now(),"13:50")
    return now > startTime and now < endTime


def todayDate():
    return datetime.datetime.now().strftime("%Y-%m-%d")


def strToTime(date, timestr):
    time = datetime.datetime.strptime(timestr, '%H:%M')
    return time.replace(year=date.year, month=date.month, day=date.day)
    

def strToDate(dateStr):
    return datetime.datetime.strptime(dateStr, "%Y-%m-%d")


def readableDate(dateStr):
    return strToDate(dateStr).strftime("%a, %d %b %Y")


def returnCurrentTime():
    return datetime.datetime.now().strftime("%H:%M:%S")