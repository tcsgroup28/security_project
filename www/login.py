import web
import sqlite3
import hashlib

web.config.debug = False

class Login:
	def GET(self):
		return render.login()
	def POST(self):
		conn=sqlite3.connect('project.db')
		c=conn.cursor()
		data=web.input()
		u=data.username
		p=hashlib.sha224((data.password).encode('utf-8')).hexdigest()
		c.execute('SELECT * FROM Accounts WHERE username=? AND password=?',(u,p,))
		user=c.fetchall()
		conn.commit()
		if user!=[]:
			if user[0][0][0]=='s':
				session.loginid=u
				session.user='student'
				return user
			elif user[0][0][0]=='m':
				session.loginid=u
				session.user='teacher'
				return user
		else:	
			return render.login()
	
render=web.template.render('templates/')

class Logout:
	def GET(self):
		session.kill()
		return ""

urls= (
	'/', 'index'
)

'''if __name__ == "__main__":
	app=www.application(urls, globals())
	session = www.session.Session(app, www.session.DiskStore('sessions'), initializer={'user': ''})
	app.run()
'''


