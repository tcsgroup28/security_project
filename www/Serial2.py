#!/usr/bin/env python3
import serial
import sqlite3
import hashlib
import datetime
import time

import Scanner

#sorry for file name :D

days= {0:'Monday',1:'Tuesday',2:'Wednsday',3:'Thursday',4:'Friday',5:'Saturday',6:'Sunday'}
def save_student(cardId, stID, name):
    c.execute("INSERT INTO Students(studentID, fullName,cardID) VALUES(?,?,?)", (stID, name, cardId,))
    conn.commit()


def register_attendance(card_id, class_id):
    unix= time.time()
    dateNtime = str(datetime.datetime.fromtimestamp(unix).strftime('%Y-%m-%d %H:%M:%S'))
    c.execute('SELECT studentID FROM Students WHERE cardID = ?', (card_id,))
    stID= data=c.fetchone()[0]
    date = dateNtime[:10]
    Time = dateNtime[11:]
    #Time='10:52:29'
    #date='2017-10-25'

    if student_already_registred(stID, class_id):
        c.execute("INSERT INTO Attendance(studentID, SignInTime, classID) VALUES(?,?,?)",(stID,Time,class_id,))
        conn.commit()
        print('Signed in!')
        Scanner.add_scan_event(True, "Student signed in!")
        ser.write('I'.encode('utf-8')) # make a sound

    elif check_signOff(stID,class_id):
        c.execute("UPDATE Attendance SET SignOffTime= ? WHERE studentID==? AND classID== ?",(Time,stID,class_id,))
        conn.commit()
        print('Signed off!')
        Scanner.add_scan_event(True, "Student signed off!")
        ser.write('O'.encode('utf-8')) # make a sound
    else:
        ser.write('N'.encode('utf-8')) # make a sound
        print('Already registered!')
        Scanner.add_scan_event(False, "Student has already registered!")


def student_already_registred(stID, classID):
    c.execute("SELECT count(*) FROM Attendance WHERE studentID == ? AND classID== ?", (stID,classID,))
    data=c.fetchone()[0]
    return data == 0


def check_signOff(stID,classID):
    c.execute("SELECT count(*) FROM Attendance WHERE studentID == ? AND classID== ? AND SignOffTime IS NOT NULL", (stID,classID,))
    data=c.fetchone()[0]
    return data == 0


def create_tables():
    c.execute('CREATE TABLE IF NOT EXISTS Students(studentID text,fullName text, cardID text,password text, PRIMARY KEY(studentID))')
    c.execute('CREATE TABLE IF NOT EXISTS Attendance(studentID text,SignInTime text,SignOffTime, classID integer, PRIMARY KEY(studentID,classID))')
    c.execute('CREATE TABLE IF NOT EXISTS Schedule(ID integer, className text, startTime text, endTime text, date text,week integer, teacherName text, room text, activityType text,courseID INTEGER, PRIMARY KEY(ID))')
    c.execute('CREATE TABLE IF NOT EXISTS Courses(courseID INTEGER, courseName text, PRIMARY KEY(courseID))')
    c.execute('CREATE TABLE IF NOT EXISTS Teachers(teacherID INTEGER, password text, fullName text, PRIMARY KEY(teacherID))')
    c.execute('CREATE TABLE IF NOT EXISTS Privileges(teacherID integer, courseID integer, PRIMARY KEY(teacherID,courseID))')


def card_not_registred(name):
    c.execute("SELECT count(*) FROM Students WHERE cardID = ?", (name,))
    data = c.fetchone()[0]
    return data == 0



global must_stop
must_stop = False

global sound_to_make
sound_to_make = None

global conn,c,ser

def make_sound(val):
    global sound_to_make
    sound_to_make = val



def millis_now():
    return int(round(time.time() * 1000))


global keep_alive
def on_keep_alive():
    global keep_alive
    keep_alive = millis_now()

def process_rfid(rfid, session_id, _conn, _c):
    global conn, c
    tmpconn = conn
    tmpc = c
    conn = _conn
    c = _c

    if card_not_registred(rfid):  # If the card is not already in the system
        print('New card detected!')
        Scanner.add_scan_event(False, "__card_id:" + rfid)
        # register_attendance(inpt, session_id, ser)
    else:
        register_attendance(rfid, session_id)
    conn = tmpconn
    c = tmpc


def scan_loop(dbPath, session_id):
    exit_reason = ""
    print("Starting scan_loop")
    global c,conn,ser, must_stop, keep_alive
    must_stop = False
    keep_alive = millis_now()
    conn = sqlite3.connect(dbPath)
    c = conn.cursor()
    create_tables() # create if tables don't exist
    try:
        #ser = serial.Serial('/dev/cu.usbmodem1411', 9600)
        ser = serial.Serial('COM7', 9600)
        must_stop = False
        while not must_stop:
            if millis_now() - keep_alive > 1000:
                must_stop = True
                exit_reason="no_keep_alive"
            if ser.inWaiting() > 0: #if there are bytes to read
                d = ser.readline()#ser.read(ser.inWaiting())[0:14]
                inpt = hashlib.sha224(d).hexdigest()
                print(inpt)
                process_rfid(inpt, session_id, conn, c)
            global sound_to_make
            if sound_to_make:
                ser.write(sound_to_make.encode('utf-8'))
                sound_to_make = None

            time.sleep(0.1) # Sleep to avoid using too much resources
        if exit_reason != "no_keep_alive": exit_reason = "must_stop"
        print("Scan loop finished!")
    except Exception as e:
        print(e)
        exit_reason = "exception"
        raise e
    finally:
        print("Closing ports!")
        print("Exit reason: " + exit_reason)
        must_stop = False
        ser.close()
        c.close()
        conn.close()