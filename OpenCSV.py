import csv
import sqlite3
courses=['Pearl-Computer Arhitecture','Pearl-Algorithmics','Pearl-Databases','Pearl-Functional programming','Pearl-Cryptography','Pearl-Computer Networks and Operating systems','Pearl-Intelligent interaction','Pearl-Requirements Engineering','Math']
def genCourses(courses):
    try:
        conn = sqlite3.connect('project.db')
        c = conn.cursor()
        for i in range(1,len(courses)+1):
            c.execute('INSERT INTO Courses(courseID,courseName) VALUES (?,?)',(i,courses[i-1],))
        conn.commit()
    finally:
        c.close()
        conn.close()
def genSchedule():
    try:
        conn = sqlite3.connect('project.db')
        c = conn.cursor()
        d = 1
        with open('timetable.csv') as csvfile:
            readerCSV = csv.reader(csvfile, delimiter=';')
            for row in readerCSV:
                if len(row) == 16 and row[0] != 'Activity':
                    c.execute(
                        "INSERT INTO Schedule(ID ,className, startTime,endTime,date,week,teacherName,room, activityType,courseID) VALUES(?,?,?,?,?,?,?,?,?,?)",
                        (d, row[0], row[4], row[7], row[3], row[1], row[10], row[11], row[9],(int(row[1])%35 if 'Pearl' in str(row[0]) else 9 if 'Math' in str(row[0]) else 0)))
                    d += 1

        conn.commit()
    finally:
        c.close()
        conn.close()
genCourses(courses)
genSchedule()

print('Done!')
